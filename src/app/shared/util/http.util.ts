import { HttpHeaders } from '@angular/common/http';
import { TokenUtil } from './token.util';

export class HttpUtil {
    public static GetHttpHeadersWithToken(): HttpHeaders {
        let httpHeaders: HttpHeaders = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + TokenUtil.GetToken()
        });
        return httpHeaders;
    }
}