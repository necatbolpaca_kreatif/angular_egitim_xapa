import { AppKeyword } from '../constant/app.keywords';

export class TokenUtil {
    public static GetToken(): string {
        return localStorage.getItem(AppKeyword.TokenKeyName);
    }

    public static SetToken(token: string): void {
        localStorage.setItem(AppKeyword.TokenKeyName, token);
    }
}