export class ApiConstant {
    private static LoginEndPoint = '/LoginToApp';
    private static CategoryListEndPoint = '/ListCategories';
    private static ProductListEndPoint = '/ListProducts';
    private static CartShowEndPoint = '/ShowMyCart';
    private static PlaceMyOrderEndPoint = '/PlaceMyOrder';
    public static ApiBaseAddress = 'http://94.138.207.170:999';
    public static LoginAddress = ApiConstant.ApiBaseAddress + ApiConstant.LoginEndPoint;
    public static CategoryListAddress = ApiConstant.ApiBaseAddress + ApiConstant.CategoryListEndPoint;
    public static ProductListAddress = ApiConstant.ApiBaseAddress + ApiConstant.ProductListEndPoint;
    public static CartShowAddress = ApiConstant.ApiBaseAddress + ApiConstant.CartShowEndPoint;
    public static PlaceMyOrderAddress = ApiConstant.ApiBaseAddress + ApiConstant.PlaceMyOrderEndPoint;
}