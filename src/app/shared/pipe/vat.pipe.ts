import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
    name: 'vat'
})
export class VATPipe implements PipeTransform {
    transform(value: number, VATRate: number = 8): number {
        return value * (VATRate / 100) + value;
    }
}