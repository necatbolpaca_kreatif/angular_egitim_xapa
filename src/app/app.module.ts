import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './component/login/login.component';
import { MainComponent } from './component/main/main.component';
import { ProductComponent } from './component/main/product/product.component';
import { CategoryComponent } from './component/main/category/category.component';
import { CartComponent } from './component/main/cart/cart.component';
import { VATPipe } from './shared/pipe/vat.pipe';
import { OrderComponent } from './component/main/order/order.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainComponent,
    ProductComponent,
    CategoryComponent,
    CartComponent,
    VATPipe,
    OrderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
