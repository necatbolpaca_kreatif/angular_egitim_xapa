import { BaseService } from 'src/app/shared/base.service';
import { HttpClient } from '@angular/common/http';
import { LoginModel } from './login.model';
import { ApiConstant } from 'src/app/shared/constant/api.constant';
import { AppKeyword } from 'src/app/shared/constant/app.keywords';
import { TokenUtil } from 'src/app/shared/util/token.util';

export class LoginService extends BaseService {
    //http üzerinden rest apiye giderek dönen değere göre login olur veya hata döndürür
    constructor(private http: HttpClient) { //Bu sekilde yazildiginda nesne icinde tanimli oluyor
        super();

    }

    public DoLogin(model: LoginModel): void {
        console.log('DoLogin');
        let jsonData: string = JSON.stringify(model);
        this.http.post(ApiConstant.LoginAddress, jsonData,{
            headers: {
                //gonderilen paketin icerik tipi json
                //application/xml, text/xml vb. olabilir.
                'Content-Type': 'application/json'
            }
        }).subscribe(result => {
            TokenUtil.SetToken(result.toString())
        });
    }
}