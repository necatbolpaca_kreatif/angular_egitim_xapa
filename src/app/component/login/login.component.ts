import { Component, OnInit } from '@angular/core';
import { LoginModel } from './login.model';
import { LoginService } from './login.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public model: LoginModel;
  constructor(private http: HttpClient) {
    this.model = new LoginModel();
    this.model.UserAppName = 'ErdoganXaPa';
    this.model.UserPassword = 'qW12345?';
  }

  ngOnInit() {
  }

  btnLogin_Click($event: MouseEvent): void {
    console.log(JSON.stringify(this.model));
    let service: LoginService = new LoginService(this.http);
    service.DoLogin(this.model);
  }

}
