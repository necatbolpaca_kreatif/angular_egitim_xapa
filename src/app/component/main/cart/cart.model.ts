export class CartModel {
    public CustomerName: string;
    public OrderCost: number;
    public OrderDate: Date;
    public OrderQuantity: number;
    public ProductCategory: string;
    public ProductName: string;

    /**
     *
     */
    constructor() {
        this.CustomerName = '';
        this.OrderCost = 0;
        this.OrderDate = new Date();
        this.OrderQuantity = 0;
        this.ProductCategory = '';
        this.ProductName = '';
    }
}