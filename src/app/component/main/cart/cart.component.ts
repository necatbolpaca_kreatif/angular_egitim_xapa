import { Component, OnInit } from '@angular/core';
import { CartModel } from './cart.model';
import { CartService } from './cart.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  public model: Array<CartModel>;

  constructor(private http: HttpClient) {
    this.model = new Array<CartModel>();
  }

  private InitCart():void {
    let service = new CartService(this.http);
    service.GetCart().subscribe(result => {
      this.model = result;
    });
  }
  ngOnInit() {
    this.InitCart();
  }

}
