import { BaseService } from 'src/app/shared/base.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CartModel } from './cart.model';
import { ApiConstant } from 'src/app/shared/constant/api.constant';
import { HttpUtil } from 'src/app/shared/util/http.util';

export class CartService extends BaseService {
    /**
     *
     */
    constructor(private http: HttpClient) {
        super();    
    }

    public GetCart(): Observable<Array<CartModel>> {
        return this.http.get<Array<CartModel>>(ApiConstant.CartShowAddress, {
            headers: HttpUtil.GetHttpHeadersWithToken()
        });

        // return this.http.get<Array<CartModel>>();
    }
}