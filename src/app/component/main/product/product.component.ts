import { Component, OnInit } from '@angular/core';
import { ProductModel } from './product.model';
import { ProductService } from './product.service';
import { HttpClient } from '@angular/common/http';
import { OrderModel } from '../order/order.model';
import { OrderService } from '../order/order.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  public model: Array<ProductModel>;
  private productService: ProductService;
  private orderService: OrderService;
  public dateModel: Date;

  constructor(private http: HttpClient) {
    this.model = new Array<ProductModel>();
    this.productService = new ProductService(this.http);
    this.productService.GetAllProducts().subscribe(result => this.model = result);
    this.dateModel = new Date(1998, 11, 5);
  }

  ngOnInit() {
  }

  public SetOrder(productId: number): void {
    console.log(productId);
    let orderModel = new OrderModel();
    //orderModel.OrderQuantity = Number.parseInt(orderQuantity);
    orderModel.ProductID = productId;
    this.orderService.SetOrder(orderModel).subscribe(result => alert(result));
  }

}
