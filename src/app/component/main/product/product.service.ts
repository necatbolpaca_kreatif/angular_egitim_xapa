import { BaseService } from 'src/app/shared/base.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ProductModel } from './product.model';
import { ApiConstant } from 'src/app/shared/constant/api.constant';
import { TokenUtil } from 'src/app/shared/util/token.util';

export class ProductService extends BaseService {
    /**
     *
     */
    constructor(private http: HttpClient) {
        super();
        
    }

    public GetAllProducts(): Observable<Array<ProductModel>> {
        return this.http.get<Array<ProductModel>>(ApiConstant.ProductListAddress,{
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Basic " + TokenUtil.GetToken()
            }
        })
    }
}