import { OrderModel } from '../order/order.model';

export class ProductModel  extends OrderModel{
    public CategoryID: number;
    // public ProductID: number;
    public ProductName: string;
    public UnitPrice: number;
    public UnitsInStock: number;
    public UnitsOnOrder: number;

    /**
     *
     */
    constructor() {
        super();
        this.CategoryID = 0;
        this.ProductID = 0;
        this.ProductName = '';
        this.UnitPrice = 0;
        this.UnitsInStock = 0;
        this.UnitsOnOrder = 0;
    }
}