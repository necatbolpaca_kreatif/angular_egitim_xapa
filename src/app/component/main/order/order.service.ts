import { BaseService } from 'src/app/shared/base.service';
import { OrderModel } from './order.model';
import { HttpClient } from '@angular/common/http';
import { ApiConstant } from 'src/app/shared/constant/api.constant';
import { HttpUtil } from 'src/app/shared/util/http.util';
import { Observable } from 'rxjs';

export class OrderService extends BaseService {
    /**
     *
     */
    constructor(private http: HttpClient) {
        super();
        
    }

    public SetOrder(model: OrderModel): Observable<object> {
        return this.http.post(ApiConstant.PlaceMyOrderAddress, JSON.stringify(model),{
            headers: HttpUtil.GetHttpHeadersWithToken()
        });
    }
}