import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  public panelName: string;
  constructor() {
    this.panelName = 'category';
  }

  ngOnInit() {
  }

  switchPanel(panelName: string): void {
    this.panelName = panelName;
  }

}
