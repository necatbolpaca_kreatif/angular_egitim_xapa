export class CategoryModel {
    public CategoryName: string;
    public CategoryId: number;

    constructor() {
        this.CategoryId = 0;
        this.CategoryName = '';
    }
}