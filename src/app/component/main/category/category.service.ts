import { HttpClient } from '@angular/common/http';
import { BaseService } from 'src/app/shared/base.service';
import { ApiConstant } from 'src/app/shared/constant/api.constant';
import { TokenUtil } from 'src/app/shared/util/token.util';
import { CategoryModel } from './category.model';
import { Observable } from 'rxjs';

export class CategoryService extends BaseService {
    constructor(private http: HttpClient) {
        super();
    }

    public GetAllCategoriesEasy(): Observable<CategoryModel[]> {
        return this.http.get<CategoryModel[]>(ApiConstant.CategoryListAddress,{
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + TokenUtil.GetToken()
            }
        });
    }

    public GetAllCategories(): Array<CategoryModel> {
        let result : CategoryModel[] = new Array<CategoryModel>();
        this.http.get(ApiConstant.CategoryListAddress,{
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + TokenUtil.GetToken()
            }
        }).toPromise()
        .then(response => {
            result = response as Array<CategoryModel>;
        })
        .catch(ex => console.log(ex));
        return result;
    }

    public GetAllCategoriesAsObservable() {
        Observable.create(obs => this.http.get(ApiConstant.CategoryListAddress,{
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + TokenUtil.GetToken()
            }
        })
        ).map(response => response as CategoryModel[]);
        
    }
}