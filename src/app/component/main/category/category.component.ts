import { Component, OnInit } from '@angular/core';
import { CategoryModel } from './category.model';
import { HttpClient } from '@angular/common/http';
import { CategoryService } from './category.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  public model: CategoryModel[];
  constructor(private http: HttpClient) {
    this.model = new Array<CategoryModel>();
  }

  ngOnInit() {
  }

  btnCategory_Click($event: MouseEvent): void {
    let categoryService = new CategoryService(this.http);
    this.model = categoryService.GetAllCategories();
    categoryService.GetAllCategoriesEasy().subscribe(x=>{
      this.model = x;
    })
    console.log(this.model);
  }

}
